from faker import Factory
import random
import sqlite3

fake = Factory.create('uk_UA')
conn = sqlite3.connect('db.sqlite')
c = conn.cursor()


def generate_students():
    sql_query = "BEGIN TRANSACTION; \n "
    for _ in range(0, 250):
        sql_query += "INSERT INTO student (name, group_id) " \
                     "VALUES ('%(name)s', '%(group)d'); \n" \
                     % {'name': fake.name(), 'group': random.randint(1, 14)}
    f = open('students.sql', 'w+')
    f.write(sql_query + "END TRANSACTION;")


def generate_grades():
    sql_query = "BEGIN TRANSACTION; \n "
    for i in range(1, 251):
        for j in range(0, 15):
            sql_query += "INSERT INTO grade (studet_id, \"value\") " \
                         "VALUES (%(id)d, %(value)d); \n" \
                         % {'id': i, 'value': random.randint(3, 5)}
    f = open('grades.sql', 'w+')
    f.write(sql_query + "END TRANSACTION;")


def show_students():
    for row in c.execute('SELECT * FROM student'):
        print(row.id)


show_students()
# generate_students()
# generate_grades()
