CREATE TABLE student_group (
  id     INTEGER PRIMARY KEY AUTOINCREMENT,
  cource INTEGER
);
CREATE TABLE student
(
  id       INTEGER PRIMARY KEY AUTOINCREMENT,
  name     TEXT            NOT NULL,
  group_id INTEGER,
  CONSTRAINT student_Group_id_fk FOREIGN KEY (group_id) REFERENCES student_group (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE TABLE cap
(
  group_id   INTEGER,
  student_id INTEGER,
  CONSTRAINT cap_Group_id_fk FOREIGN KEY (group_id) REFERENCES student_group (id),
  CONSTRAINT cap_student_id_fk FOREIGN KEY (student_id) REFERENCES student (id)
);
CREATE TABLE grade (
  id        INTEGER PRIMARY KEY AUTOINCREMENT,
  studet_id INTEGER,
  value     INTEGER,
  CONSTRAINT student_Group_id_fk FOREIGN KEY (studet_id) REFERENCES student (id)
);



